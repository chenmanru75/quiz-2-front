import $ from 'jquery';

export function renderEducation(educations) {
  const item = educations;
  for (let i = 0; i < item.length; i += 1) {
    $('.eductionContent').append(
      `
        <li class="eductionContentList">
          <div id="year">${item[i].year}</div>
          <div id="content">
            <h3>${item[i].title}</h3>
            <p>${item[i].description}</p>
          </div>
        </li>
      `
    );
  }
}
