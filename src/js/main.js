import fetchData from './utils/fetchData';
import createPerson from './utils/createPerson';
import { renderHeader } from './view/renderHeader';
import { renderEducation } from './view/renderEducation';
import { renderAboutMe } from './view/renderAboutme';

fetchData('http://localhost:3000/person')
  .then(result => {
    const { name, age, description, educations } = createPerson(result);
    renderHeader(name, age);
    renderAboutMe(description);
    renderEducation(educations);
  })
  .catch(error => new Error(error));
