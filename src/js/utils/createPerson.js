import { Person } from '../Person';

export default function createPerson(response) {
  const { name, age, description, educations } = response;
  return new Person(name, age, description, educations);
}
