export default function fetchData(url) {
  return fetch(url).then(response => {
    return response.json();
  });
}
